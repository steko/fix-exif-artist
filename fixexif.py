#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
from collections import defaultdict

import pyexiv2

try:
    # this raw_input is not converted by 2to3
    term_input = raw_input
except NameError:
    term_input = input

ROOT_DIRECTORY = os.environ['EXIF_ROOT_DIR']

AUTHORS = {"Canon PowerShot A480": "Stefania Picciola",
           #"Canon PowerShot A520": "",
           "Canon EOS 500D": "Francesco Ripanti",
           "SAMSUNG ES74,ES75,ES78 / VLUU ES75,ES78 ": "Maria Sole Distefano",
           "DSC-W50": "Samanta Mariotti",
           "NIKON D50": "Alessandro Carabia",
           #"NIKON D80": "",
           }

def list_models(directory):
    '''Lists all the camera models from EXIF metadata.

    Can be used to present the user a prompt to type the artist name for
    each model, instead of saving a static dictionary.'''

    d = directory
    models = defaultdict(int)
    # FIXME move this double loop to a separate function, or better algorithm
    for i in os.walk(d):
        for f in i[2]:
            fp = os.path.join(i[0],f)
            metadata = pyexiv2.ImageMetadata(fp)
            metadata.read()
            try:
                model = metadata['Exif.Image.Model'].value
            except KeyError:
                pass
            else:
                models[model] += 1
    return models

def fixexif(directory, matches):
    '''Changes EXIF metadata based on a dictionary.'''

    d = directory
    # FIXME move this double loop to a separate function, or better algorithm
    for i in os.walk(d):
        for f in i[2]:
            fp = os.path.join(i[0],f)
            metadata = pyexiv2.ImageMetadata(fp)
            metadata.read()
            model = metadata['Exif.Image.Model'].value
            try:
                artist = matches[model]['artist']
            except KeyError:
                #print('Error: could not find author for %s' % fp)
                pass
            else:
                metadata['Exif.Image.Artist'] = artist
                metadata.write()

def match_artists(directory):
    models = list_models(directory)
    matches = dict()

    # TODO save these matches in the home directory and use them again as
    # defaults next time
    for model, count in models.items():
        try:
            x = term_input('Enter artist name for camera model "%s" (%d pictures): ' % (model, count))
        except KeyboardInterrupt:
            sys.exit()
        else:
            matches[model] = {'count': count, 'artist': x}
    fixexif(directory, matches)

if __name__ == '__main__':
    match_artists(ROOT_DIRECTORY)
